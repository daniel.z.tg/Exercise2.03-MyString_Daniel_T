
/**
 * A bunch of useful String operations.
 * 
 * @author Daniel Tang
 * @since 10 April 2017
 */
public final class MyStrings {

	/**
	 * Converts a data String in American style to Canadian style.
	 * 
	 * @param americanDate A String of a date in American style
	 * @return A String of a date in Canadian style.
	 */
	public static final String convertDate(final String americanDate) {
		final String[] parts = americanDate.split("/");

		if (parts.length != 3) {
			throw new IllegalArgumentException("Expected date in format mm/dd/yyyy");
		}

		// The difference between the styles is that the two first numbers are switched.
		return parts[1] + "-" + parts[0] + "-" + parts[2];
	}

	/**
	 * Converts a human-friendly phone number to the keys needed to be pressed on the dialpad.
	 * 
	 * @param humanPhoneNum A String of a human-friendly phone number
	 * @return A long representing the number spelled when the keys needed to be pressed are written out.
	 */
	public static final long convertPhoneNum(final String humanPhoneNum) {
		try {
			return Long.parseLong(
					humanPhoneNum.replaceAll("[^0-9]", "")); // Remove all human-friendly formatting, leaving only numbers behind
		} catch (final NumberFormatException nfe) {
			throw new IllegalArgumentException("Expected phone number in formats like (xxx)xxx-xxxx", nfe);
		}
	}

	/**
	 * Converts a Latin simple name to a formal name.
	 * 
	 * @param simpleName A String of a Latin simple name
	 * @return A String of a Latin formal name
	 */
	public static final String convertName(final String simpleName) {
		final String[] parts;

		{
			String tmp = simpleName
					.replaceAll("[^a-zA-Z ]", "") // Remove all non-Latin formatting (may remove accents)
					.replaceAll(" +", " "); // Remove double spaces

			if (tmp.length() < 3) {
				throw new IllegalArgumentException("Expected name in format: \"First Last\"");
			}

			// Remove all spaces before
			if (tmp.startsWith(" ")) {
				tmp = tmp.substring(1);
			}

			// and after
			if (tmp.endsWith(" ")) {
				tmp = tmp.substring(0, tmp.length() - 1);
			}

			parts = tmp.split(" "); // Split into name parts
		}

		if (parts.length != 2) {
			throw new IllegalArgumentException("Expected name in format: \"First Last\"");
		}

		// Formal name is first and last name switched, with comma and space inserted between.
		return parts[1] + ", " + parts[0];
	}

	/**
	 * Calculates a String with the reverse character order of a input String.
	 * 
	 * @param str The String to calculate the reverse of
	 * @return The String with the reverse character order of the input String
	 */
	public static final String reverseString(final String str) {
		final int length = str.length();
		char[] result = new char[length];

		for (int i = 0; i < length; i++) {
			// Fill in the characters backwards in a loop
			result[length - i - 1] = str.charAt(i);
		}

		return new String(result);
	}

	/**
	 * Determines if two Strings are anagrams of one another.
	 * 
	 * @param s1 The first String
	 * @param s2 The other String
	 * @return {@code true} if the two String are anagrams of one another.
	 */
	public static final boolean areAnagrams(final String s1, final String s2) {
		/*
		 * An array of balances is used to determine anagrams.
		 * Each element of the array is an English alphabet letter.
		 * One String tips the balance one way, the other String, the other.
		 * 
		 * If they do indeed contain the same number of characters,
		 * then the both Strings' tipping will cancel the other's out,
		 * and we will end up with a balanced balance.
		 */
		int[] balances = new int['z' - 'a' + 1];

		for (final char c : s1.toLowerCase() // Don't care about case
				.replaceAll("[^a-z]", "") // Throw away all formatting
				.toCharArray()) {
			balances[c - 'a']++; // Tip one way
		}

		for (final char c : s2.toLowerCase().replaceAll("[^a-z]", "").toCharArray()) {
			balances[c - 'a']--; // Tip the other way
		}

		// Loop through, and check if there is any imbalance
		for (final int i : balances) {
			if (i != 0) {
				return false;
			}
		}

		return true;
	}

	private MyStrings() { throw new UnsupportedOperationException(); }
}
